#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from Netflix import helps
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):
    # ----
    # eval
    # ----
    def test_helps_1(self):
        id = 10040
        cache = { (10040, 1999): 2.4}
        pred = helps(id, cache)
        self.assertEqual(pred, 2.4)
    def test_helps_2(self):
        id = 10040
        cache = { (10040, 2000): 2.4}
        pred = helps(id, cache)
        self.assertEqual(pred, 2.4)
    def test_helps_3(self):
        id = 10040
        cache = { (10040, 2001): 2.4}
        pred = helps(id, cache)
        self.assertEqual(pred, 2.4)


    def test_eval_1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.68\n3.47\n3.62\n0.50\n")
    def test_eval_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10:\n3.3\n3.17\n0.24\n")
    def test_eval_3(self):
        r = StringIO("1000:\n2326571\n977808\n1010534\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1000:\n3.53\n3.27\n3.13\n0.73\n")

    
# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
