#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, predict, get_cached_values, caches
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):
    # ----
    # get_cached_values
    # ----
    test_cache = get_cached_values(caches, 2149204, 3492)
    
    def test_in_cache(self):
        self.assertIsNotNone(TestNetflix.test_cache)


    def test_cache_structure(self):
        cache_schema = {
            'customer_yearly_average': 4.115, 
            'm_year_adjust': 2.99271003558,
            'm_adjust': 2.96171003558, 
            'customer_average': 3.89, 
            'actual_customer_rating': 5
        }
        
        self.assertEqual(TestNetflix.test_cache, cache_schema)

    def test_not_in_cache(self):
        self.assertEqual(get_cached_values(caches, -1, -1), {})

    # ----
    # predict
    # ----

    def test_predict_1(self):
        self.assertEqual(predict(TestNetflix.test_cache), 3.5127420071160005)
        
    def test_predict_empty(self):
        with self.assertRaises(KeyError):
            predict({})

    def test_predict_big(self):
        big_cache = {
            'customer_yearly_average': 10, 
            'm_year_adjust': 10,
            'm_adjust': 10, 
            'customer_average': 10, 
            'actual_customer_rating': 5
        }

        self.assertEqual(predict(big_cache), 5)

    def test_predict_fixed(self):
        # should return mean of 1 & AVERAGE_RATING
        self.assertEqual(predict(TestNetflix.test_cache), 3.5127420071160005)
        
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.1\n3.1\n3.4\n0.90\n")

    def test_eval_2(self):
        r = StringIO("2043:\n1417435\n2312054\n462685\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "2043:\n3.7\n3.4\n3.8\n1.45\n")

    def test_eval_rmse(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        rmse = w.getvalue().split("\n")[-2]
        assert float(rmse) < 1.00   

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          50      3      8      2    91%   57-58, 106, 56->57, 105->106
TestNetflix.py      41      0      0      0   100%
------------------------------------------------------------
TOTAL               91      3      8      2    95%
"""
